/** @type {import('next').NextConfig} */
const nextConfig = {
    headers: async () => {
        return [
            {
                source: '/(.*)',
                headers: [
                    {
                        key: "Cross-Origin-Opener-Policy",
                        value: "same-origin",
                    },
                    {
                        key: "Cross-Origin-Embedder-Policy",
                        value: "require-corp",
                    },
                    // {
                    //     key: 'X-Frame-Options',
                    //     value: 'DENY',
                    // },
                    // {
                    //     key: 'X-XSS-Protection',
                    //     value: '1; mode=block',
                    // },
                    // {
                    //     key: 'X-Content-Type-Options',
                    //     value: 'nosniff',
                    // },
                    // {
                    //     key: 'Referrer-Policy',
                    //     value: 'origin-when-cross-origin',
                    // },
                    // {
                    //     key: 'Content-Security-Policy',
                    //     value: "default-src 'self'; img-src 'self' data:; script-src 'self' 'unsafe-inline' 'unsafe-eval'; style-src 'self' 'unsafe-inline'; connect-src 'self'; frame-ancestors 'none'; base-uri 'self'; form-action 'self';",
                    // },
                    // {
                    //     key: 'Permissions-Policy',
                    //     value: 'camera=(), microphone=(), geolocation=(), interest-cohort=()',
                    // },
                ],
            },
        ]
    }
}

module.exports = nextConfig
